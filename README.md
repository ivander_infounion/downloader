## Installation

### Requirements
 - Vagrant (via VirtualBox provider)


### Deploy
 - clone repository
 -      vagrant up
 -      vagrant ssh - to connect virtual server
 -      cd /app  - to get to app root folder 
  

## Project

### Api

Use postman to quick-view. (file Downloader.postman_collection.json in root)

POST http://downloader.loc/api/v1/files - add file to download
GET http://downloader.loc/api/v1/files - list all files
GET http://downloader.loc/api/v1/files/1 - fetch info about file with id 1

### Console

 - ./artisan downloader:add {DownloadUrl} - Add file to download queue.
 - ./artisan downloader:all - All files with statuses

### Web
 - http://downloader.loc/files - all files list
 - http://downloader.loc/files/create - download new file
 
### Tests
 - run 'vendor/bin/phpunit' in /app
