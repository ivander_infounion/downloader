<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DownloaderStructureTables extends Migration
{

    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('name')->nullable();
            $table->string('real_name')->nullable();
            $table->dateTime('created')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('downloaded')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('files');
    }
}
