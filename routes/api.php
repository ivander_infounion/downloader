<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1','namespace' => 'Api'], function () {
    Route::name('api.')->group(function () {
        Route::apiResource('files', 'FileController')->parameter('files', 'id');
        Route::get('files/{id}/get', 'FileController@download')->name('files.get');
    });
});
