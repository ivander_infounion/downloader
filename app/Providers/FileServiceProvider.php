<?php

namespace App\Providers;

use App\Services\File;
use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton('services.file', function ($app) {
            return new File();
        });
    }
}
