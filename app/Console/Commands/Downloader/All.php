<?php

namespace App\Console\Commands\Downloader;

use App\Models\File;
use Illuminate\Console\Command;
use Psr\Container\ContainerInterface;

class All extends Command
{
    protected $signature = 'downloader:all';
    protected $description = 'All files with statuses';

    private $fileService;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->fileService = $container->get('services.file');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $output = [];
        $files = File::all();
        if (!empty($files)) {
            foreach ( $files as $file) {
                $output[] = [
                    $file->id,
                    $file->url,
                    $file->getStatusTitle(),
                    $file->name,
                    $file->created,
                    $file->downloaded,
                ];
            }
        }
        $this->table([
            'Id',
            'Url',
            'Status',
            'Name',
            'Created',
            'Downloaded',
        ], $output);
    }
}
