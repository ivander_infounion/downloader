<?php

namespace App\Console\Commands\Downloader;

use Illuminate\Console\Command;
use Psr\Container\ContainerInterface;

class add extends Command
{
    protected $signature = 'downloader:add {url}';
    protected $description = 'Add file to download queue.';

    private $fileService;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->fileService = $container->get('services.file');
    }

    public function handle(): void
    {
        $url = $this->argument('url');

        try {
            $this->fileService->download($url);
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
        $this->info('Added successfully');
    }
}
