<?php
namespace App\Services;

use App\Exceptions\CantBeDownloaded;
use App\Jobs\FileDownloadJob;
use App\Models\File as FileModel;
use GuzzleHttp\Exception\ConnectException;
use Validator;

class File
{
    const OK_STATUS_CODES = [
        200,
    ];

    private $client;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function download(string $url) : ?FileDownloadJob
    {
        $validator = Validator::make(['url' => $url], ['url' => ['url', 'required']]);
        try {
            if ($validator->passes() && $this->isReachable($url)) {
                /** @var \App\Models\File $fileObject */
                $fileObject = FileModel::create(['url' => $url]);
                $fileObject->save();
                FileDownloadJob::dispatch($fileObject);
            } else {
                throw new CantBeDownloaded('Url is not valid');
            }
        } catch (ConnectException $e) {
            throw new CantBeDownloaded('Url is valid but unreachable');
        }
        return $job ?? null;
    }

    private function isReachable(string $url) :bool
    {
        return in_array($this->client->head($url)->getStatusCode(), self::OK_STATUS_CODES);
    }

    public function getDownloadUrl(FileModel $file): string
    {
        return route('api.files.get', $file->id);
    }
}
