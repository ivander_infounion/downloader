<?php

namespace App\Jobs;

use App\Models\File;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Storage;

class FileDownloadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileModel;

    public function __construct($fileModel)
    {
        $this->fileModel = $fileModel;
    }

    public function handle()
    {
        if (!$this->check()) {
            return null;
        }
        $this->download();
    }

    public function download()
    {
        $this->getFile()->initDownload();
        $fileName = md5(time());
        $tmpPath =  Storage::path($fileName);
        $resource = fopen($tmpPath, 'w');
        (new Client())->get($this->getFile()->url, ['save_to' => $resource]);
        if (is_file($tmpPath)) {
            // validate file before save, some files may cause data leaks
            // saving to storage
            if (!Storage::move($fileName, $this->getFile()->getPath())) {
                $this->getFile()->setDownloadedStatus(false);
            }
        }
        $this->getFile()->setDownloadedStatus(true);
        $this->getFile()->save();
    }


    private function check()
    {
        return $this->getFile() !== null
            && $this->getFile()->isWaitingForDownload();
    }

    public function getFile()
    {
        if ($this->fileModel === null) {
            $this->fileModel = File::find($this->id);
        }
        return $this->fileModel;
    }
}
