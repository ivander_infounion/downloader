<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * App\Models\File
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $url
 * @property string $name
 * @property string $real_name
 * @property string|null $created
 * @property string|null $downloaded
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereDownloaded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereIsDownloader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereRealName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereIsDownloaded($value)
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereStatus($value)
 */
class File extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_DOWNLOADING = 1;
    const STATUS_COMPLETE = 2;
    const STATUS_ERROR = 3;

    const STATUS_NAMES = [
        self::STATUS_PENDING => 'pending',
        self::STATUS_DOWNLOADING => 'downloading',
        self::STATUS_COMPLETE => 'complete',
        self::STATUS_ERROR => 'error',
    ];

    public $timestamps = false;

    protected $hidden = ['real_name'];

    protected $fillable = [
        'url',
        'name',
        'status',
        'real_name',
        'created',
        'downloaded',
    ];

    protected $casts = [
        'created' => 'datetime',
        'downloaded' => 'datetime',
    ];

    public function setDownloadedStatus(bool $status = false)
    {
        $this->update([
            'status' => $status ? self::STATUS_COMPLETE : self::STATUS_ERROR,
            'downloaded' => date('Y-m-d H:i:s'),
            'is_downloaded' => (int) $status,
        ]);
    }

    public function isWaitingForDownload()
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function hasFile()
    {
        return is_file(Storage::path($this->getPath()));
    }

    public function initDownload()
    {
        $this->update([
            'status' => self::STATUS_DOWNLOADING,
            'downloaded' => date('Y-m-d H:i:s'),
            'real_name' => $this->name = basename($this->url),
        ]);
    }

    public function getStatusTitle()
    {
        return self::STATUS_NAMES[$this->status] ?? '';
    }

    public function getDownloadUrl()
    {
        return self::STATUS_NAMES[$this->status] ?? '';
    }


    public function getPath()
    {
        return 'file-' . $this->id . '-' . $this->real_name;
    }

}
