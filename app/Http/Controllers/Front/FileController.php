<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileAddRequest;
use App\Models\File;
use Illuminate\Http\Request;
use Psr\Container\ContainerInterface;

class FileController extends Controller
{
    private $fileService;

    public function __construct(ContainerInterface $container)
    {
        $this->fileService = $container->get('services.file');
    }

    public function index(Request $request)
    {
        return view('file.index')->with('files', File::all());
    }

    public function store(FileAddRequest $request)
    {
        $this->fileService->download($request->get('url'));
        session()->flash('added!', 'Download has been added to queue');
        return view('file.new');
    }

    public function create(Request $request)
    {
        return view('file.new');
    }



}
