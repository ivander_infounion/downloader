<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\File\FileAddRequest;
use App\Http\Requests\Api\File\FileGetRequest;
use App\Models\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\File as FileResource;


class FileController extends Controller
{

    private $fileService;

    /**
     * Inject service to controller as singleton
     *
     * @param ContainerInterface $container
     *
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->fileService = $container->get('services.file');
    }

    public function store(FileAddRequest $request): Response
    {
        try {
            $result = $this->fileService->download($request->get('url'));
        } catch (\Exception $e) {
            $errorText = $e->getMessage();
        } finally {
            return response([
                'success' =>  isset($result),
                'message' => $errorText ?? 'File added to download queue.',
            ], JsonResponse::HTTP_CREATED);
        }
    }

    /**
     * @param FileGetRequest $request
     * @param int $id
     *
     * @return FileResource
     */
    public function show(FileGetRequest $request, int $id): FileResource
    {
        return FileResource::make(File::find($id));
    }

    public function download(FileGetRequest $request, int $id): BinaryFileResponse
    {
        return FileResource::make(File::find($id))->download();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return FileResource::collection(File::all());
    }
}
