<?php

namespace App\Http\Resources;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Storage;

class File extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            'download_link' => $this->resource->status === \App\Models\File::STATUS_COMPLETE
                ? resolve('services.file')->getDownloadUrl($this->resource)
                : null,
            'status' => $this->resource->getStatusTitle(),

        ]);
    }

    /**
     * @return BinaryFileResponse
     */
    public function download(): BinaryFileResponse
    {
        return response()->download(
            Storage::path($this->resource->getPath()),
            $this->resource->name
        );
    }
}
