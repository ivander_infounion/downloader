<?php

namespace App\Http\Requests\Api\File;

use App\Http\Requests\AbstractApiRequest;

class FileGetRequest extends AbstractApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => [ 'exists:files,id', 'required'],
        ];
    }
}
