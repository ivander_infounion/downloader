<?php

namespace Tests\Unit\Services;

use App\Exceptions\CantBeDownloaded;
use App\Jobs\FileDownloadJob;
use App\Services\File;
use Tests\TestCase;
use Mockery;
use Illuminate\Validation\Validator;

class ResourceTest extends TestCase
{

    protected $fileDownloadJob;
    protected $fileDownloadJobMock;
    protected $fileService;
    protected $fileServiceMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileService = resolve('services.file');

        $this->downloaderJobMocked = Mockery::mock(FileDownloadJob::class);
        $this->resourceServiceMocked = Mockery::mock(File::class);
    }

    public function fakeUrlsArray(): array
    {
        return [
            ['lol'],
            ['https://kek'],
            [1],
        ];
    }

    /**
     * @param string $url
     * @dataProvider fakeUrlsArray
     */
    public function testWrongUrlValidationWorks(string $url): void
    {
        $this->createMock(Validator::class)->method('validateActiveUrl')->willReturn(false);
        $this->expectException(CantBeDownloaded::class);
        $this->fileService->download($url);
    }
}
