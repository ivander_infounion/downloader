<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Download from Url</title>
    <script src="{{ asset('css/app.css') }}" defer></script>
</head>
<body>
<div class="container">
    @yield('content')
</div>
</body>
</html>
