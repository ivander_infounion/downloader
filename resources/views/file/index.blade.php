@extends('layout')
@section('content')
    <h1>All downloaded files - <a href="/files/create">Add new</a></h1>
    
    @if (count($files))
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">File Url</th>
                <th scope="col">Download Url</th>
                <th scope="col">Status</th>
                <th scope="col">Started</th>
                <th scope="col">Downloaded</th>
            </tr>
            </thead>
            <tbody>
            @foreach($files as $file)
                <tr>
                    <td>{{ $file->url }}</td>
                    <td class="text-justify">
                        @if ($file->hasFile())
                            <a href="{{ app('services.file')->getDownloadUrl($file) }}">
                                Get file
                            </a>
                        @endif
                    </td>
                    <td>{{ $file->getStatusTitle() }}</td>
                    <td>{{ $file->created }}</td>
                    <td>{{ $file->downloaded }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
