@extends('layout')
@section('content')
    <h1 class="title">Add file to download</h1>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    @endif

    @if ($message = Session::get('added!'))
        <h2>
            New file added successfully!
            <a href="/files">show</a>
        </h2>
    @endif
    <form method="POST" action="/files">
        {{ csrf_field() }}
        <input type="text" placeholder="Download Url" id="url" name="url" required>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
@endsection
